using FluentValidation;

namespace Application.Validators
{
    public static class ValidatorsExtensions
    {
        public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            var options = ruleBuilder.NotEmpty()
                    .MinimumLength(7).WithMessage("Must be at least 7 characters")
                    .Matches("[A-Z]").WithMessage("Must have 1 uppercase")
                    .Matches("[a-z]").WithMessage("Must have 1 lowercase")
                    .Matches("[0-9]").WithMessage("Must have 1 digit")
                    .Matches("[^a-zA-Z0-9]").WithMessage("Must have 1 digit");

            return options;
        }

    }
}