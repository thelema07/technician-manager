using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using MediatR;
using Persistence;

namespace Application.Requests
{
    public class Details
    {
        public class Query : IRequest<Request>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, Request>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Request> Handle(Query request, CancellationToken cancellationToken)
            {
                var requestByDetail = await _context.Requests.FindAsync(request.Id);

                if (requestByDetail == null)
                    throw new RestException(HttpStatusCode.NotFound, new { request = "Not Found :(" });

                return requestByDetail;
            }
        }
    }
}