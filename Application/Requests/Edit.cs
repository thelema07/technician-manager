using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using FluentValidation;
using MediatR;
using Persistence;

namespace Application.Requests
{
    public class Edit
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public Guid TechnicianId { get; set; }
            public Guid RequesterId { get; set; }
            public string TechnicianName { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public DateTime? StartDate { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.TechnicianName).NotEmpty();
                RuleFor(x => x.Title).NotEmpty();
                RuleFor(x => x.Description).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var editRequest = await _context.Requests.FindAsync(request.Id);

                if (editRequest == null)
                    throw new RestException(HttpStatusCode.NotFound, new { request = "Not Found :(" });

                editRequest.TechnicianName = request.TechnicianName ?? editRequest.TechnicianName;
                editRequest.Title = request.Title ?? editRequest.Title;
                editRequest.Description = request.Description ?? editRequest.Description;

                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem Saving Changes");
            }
        }
    }
}