using System;

namespace Domain
{
    public class Request
    {
        public Guid Id { get; set; }
        public Guid TechnicianId { get; set; }
        public Guid RequesterId { get; set; }
        public string TechnicianName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
    }
}