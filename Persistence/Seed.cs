using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Identity;

namespace Persistence
{
    public class Seed
    {
        public static async Task SeedData(DataContext context, UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var users = new List<AppUser>
                {
                    new AppUser
                    {
                        DisplayName = "Horus",
                        UserName = "horus",
                        Email = "horus@test.com"
                    },
                    new AppUser
                    {
                        DisplayName = "Osiris",
                        UserName = "osiris",
                        Email = "osiris@test.com"
                    },
                    new AppUser
                    {
                        DisplayName = "Thoth",
                        UserName = "thoth",
                        Email = "thoth@test.com"
                    }
                };
                foreach (var user in users)
                {
                    await userManager.CreateAsync(user, "Pa$$w0rd");
                }
            }

            if (!context.Requests.Any())
            {
                var requests = new List<Request>
                {
                    new Request
                    {
                        Id = Guid.NewGuid(),
                        TechnicianId = Guid.NewGuid(),
                        RequesterId = Guid.NewGuid(),
                        TechnicianName = "Danny Dev",
                        Title = "I am Starting to do production Jon on .Net :) :)",
                        Description = "This is a big one, but you're the man",
                        StartDate = DateTime.Now.AddMonths(-2)
                    },
                    new Request
                    {
                        Id = Guid.NewGuid(),
                        TechnicianId = Guid.NewGuid(),
                        RequesterId = Guid.NewGuid(),
                        TechnicianName = "Horus",
                        Title = "I am the God of the Skies",
                        Description = "Horus or Her, Heru, Hor in Ancient Egyptian, is one of the most significant ancient Egyptian deities who served many functions, most notably god of kingship and the sky. He was worshipped from at least the late prehistoric Egypt until the Ptolemaic Kingdom and Roman Egypt. Different forms of Horus are recorded in history and these are treated as distinct gods by Egyptologists.",
                        StartDate = DateTime.Now.AddMonths(-2)
                    },
                    new Request
                    {
                        Id = Guid.NewGuid(),
                        TechnicianId = Guid.NewGuid(),
                        RequesterId = Guid.NewGuid(),
                        TechnicianName = "Thoth",
                        Title = "I am the God of Writing, Magic, Wisdom, and the Moon.",
                        Description = "He was classically depicted as a green-skinned deity with a pharaoh's beard, partially mummy-wrapped at the legs, wearing a distinctive atef crown, and holding a symbolic crook and flail.",
                        StartDate = DateTime.Now.AddMonths(-2)
                    },
                    new Request
                    {
                        Id = Guid.NewGuid(),
                        TechnicianId = Guid.NewGuid(),
                        RequesterId = Guid.NewGuid(),
                        TechnicianName = "Osiris",
                        Title = "I am the God of Fertility, Agriculture, the Afterlife, the Dead, Resurrection, Life, and Vegetation in ancient Egyptian religion.",
                        Description = "He was classically depicted as a green-skinned deity with a pharaoh's beard, partially mummy-wrapped at the legs, wearing a distinctive atef crown, and holding a symbolic crook and flail.",
                        StartDate = DateTime.Now.AddMonths(-2)
                    }
                };

                context.Requests.AddRange(requests);
                context.SaveChanges();
            }
        }
    }
}