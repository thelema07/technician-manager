## Technician Manager

Web application to connect office users with IT Technicians, assign distribute and track IT Requests

---

## Technologies Used

1. **C#**,**Net Core** **Entity Framework** for Backend.
2. **Typescript**,**ReactJS** **Mobx** for Frontend.
3. **Postman** Query verification.

---

## useful dotnet cli commands

1. _dotnet --version_ => lists the active version for the project.
2. _dotnet -h_ => lists all the avaiable commands included in the SDK.
3. _dotnet add -h_ => List all tha posible preinstalled packages we can run in the cli.
4. _dotnet sln list_ => List all the projects linked to the solution.
5. _dotnet add reference_ => this command is used to link dependencies inside a solution(sln)
6. _dotnet restore_ => restore packages and dependencies after installing or for debugging purposes
7. _dotnet watch run_ => starts the app ands watchs for changes.
8. _dotnet ef migrations add [Migrationname]_ => used to implement migrations using dependencies of the solution.
9. _dotnet ef migrations remove [Migrationname]_ => used to remove migrations using dependencies of the solution.
10. _dotnet ef migrations -h_ => contextual help.
